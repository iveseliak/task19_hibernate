package ua.com.mysite.entity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import repository.CityRepository;
import ua.com.mysite.project.City;

public class Main {

	public static void main(String[] args) {

		EntityManagerFactory factory = 
				Persistence.createEntityManagerFactory("primary");
		EntityManager manager = factory.createEntityManager();
		
	manager.getTransaction().begin();
	
//*********************1Task*************************
	
	CityRepository city1 = new CityRepository(manager);
	City city1_1 = new City();
	city1_1.setName("Lviv");
	city1.save(city1_1);
	
	CityRepository city2 = new CityRepository(manager);
	City city1_2 = new City();
	city1_2.setName("New-York");
	city2.save(city1_2);
	
	City city = (City) manager.createQuery("select p from City p where p.id=:nameId").setParameter("nameId",new Long(5)).getSingleResult();
	System.out.println(city.getName());
		
	
/*********************2-3Task*************************/
//	PersonRepository p1 = new PersonRepository(manager);
//	Person p1_1 = new Person();
//	p1_1.setName("Vasyl");
//	p1.save(p1_1);
//	Person p1_2 = new Person();
//	p1_2.setName("Ivan");
//	p1.save(p1_2);
//	Person p1_3 = new Person();
//	p1_3.setName("Igor");
//	p1.save(p1_3);
	/*
	PersonRepository p1 = new PersonRepository(manager);
	ArrayList<Person> personList = new ArrayList<Person>(); 
	personList.add((Person) manager.createQuery("select p from Person p where p.id=:nameId").setParameter("nameId",new Long(7)).getSingleResult());
	personList.add((Person) manager.createQuery("select p from Person p where p.id=:nameId").setParameter("nameId",new Long(9)).getSingleResult());
	personList.add((Person) manager.createQuery("select p from Person p where p.id=:nameId").setParameter("nameId",new Long(10)).getSingleResult());
	personList.add((Person) manager.createQuery("select p from Person p where p.id=:nameId").setParameter("nameId",new Long(11)).getSingleResult());
	personList.add((Person) manager.createQuery("select p from Person p where p.id=:nameId").setParameter("nameId",new Long(12)).getSingleResult());
	personList.add((Person) manager.createQuery("select p from Person p where p.id=:nameId").setParameter("nameId",new Long(13)).getSingleResult());
	personList.add((Person) manager.createQuery("select p from Person p where p.id=:nameId").setParameter("nameId",new Long(14)).getSingleResult());
	personList.add((Person) manager.createQuery("select p from Person p where p.id=:nameId").setParameter("nameId",new Long(15)).getSingleResult());
	
	Object[] o = personList.toArray();
	p1.remove(o[0],o[1],o[2],o[3],o[4],o[5],o[6],o[7]);
*/
/*********************4Task*************************
	PersonRepository p1 = new PersonRepository(manager);
	p1.getPerson(2);
*/
		
//	InfoRepository i1 = new InfoRepository(manager);
//	i1.addInfoToThePerson(1L,"Washington","987654321");
	
	
		
	
			
		manager.getTransaction().commit();
		manager.close();
		factory.close();
		
		
	}

}
