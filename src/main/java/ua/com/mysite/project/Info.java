package ua.com.mysite.project;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;

@Entity
public class Info {
	
	@Id
	@GeneratedValue
	private Long id;
	private String adress;
	private String phoneNumber;
	
	
	@ManyToMany
	private List<City> infoList = new ArrayList<City>();
	
	@OneToMany
	@MapsId
	private Person person;
	public Info() {}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public List<City> getInfoList() {
		return infoList;
	}
	public void setInfoList(List<City> infoList) {
		this.infoList = infoList;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}


	
	
	

}
