package ua.com.mysite.project;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
public class Person {
	
	@Id
	@GeneratedValue
	private Long Id;
	
	private String name;
	
	@OneToOne
	@PrimaryKeyJoinColumn
	private Info info;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Info getInfo() {
		return info;
	}

	public void setInfo(Info info) {
		this.info = info;
	}

	public Person() {
		
	}

	@Override
	public String toString() {
		return "Person [Id=" + Id + ", name=" + name + ", info=" + info + "]";
	}
	
	
	
	
	

}
