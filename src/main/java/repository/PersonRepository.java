package repository;

import javax.persistence.EntityManager;

import ua.com.mysite.project.Person;
	
	public class PersonRepository {
	private final EntityManager manager;
		
		public PersonRepository(EntityManager manager) {
			this.manager = manager;
	}
	
	public void save(Person person) {
		manager.getTransaction().begin();
		manager.persist(person);
	    manager.getTransaction().commit();   	
	}

	public void remove(Object object, Object object2, Object object3, Object object4, Object object5, Object object6, Object object7, Object object8) {
		manager.getTransaction().begin();
		manager.remove(object);
		manager.remove(object2);
		manager.remove(object3);
		manager.remove(object4);
		manager.remove(object5);
		manager.remove(object6);
		manager.remove(object7);
		manager.remove(object8);
	    manager.getTransaction().commit();   
	    manager.close();	
		
	}
	
	 public void getPerson(long id) {
		    manager.getTransaction().begin();
		    System.out.println(manager.createQuery("select p from Person p where p.id=:Nid").setParameter("Nid",new Long(id)).getSingleResult());	    
		    manager.getTransaction().commit();
		    manager.close();    
	}
}

