package repository;

import javax.persistence.EntityManager;

import ua.com.mysite.project.Person;
import ua.com.mysite.project.Info;
public class InfoRepository {
	private final EntityManager manager;
	public InfoRepository(EntityManager manager) {
		this.manager=manager;
	}
	
	public void addInfoToPerson(Long id, String adress,String num) {
		manager.getTransaction().begin();
		Person person1=(Person) manager.createQuery("select p from Person p where p.id=:nameId").setParameter("nameId", id).getSingleResult();
		Info info=new Info();
		info.setAdress(adress);
		info.setPhoneNumber(num);
		info.setPerson(person1);
		manager.persist(info);
		manager.getTransaction().commit();
		manager.clear();
		
	}
	
}
