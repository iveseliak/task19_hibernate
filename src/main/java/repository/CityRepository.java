package repository;

import javax.persistence.EntityManager;

import ua.com.mysite.project.City;

public class CityRepository {
	
	private final EntityManager manager;
	public CityRepository(EntityManager manager) {
		this.manager=manager;
		
	}
	public void save(City city) {
		manager.getTransaction().begin();
		manager.persist(city);
		manager.getTransaction().commit();
		
	}
	
}
